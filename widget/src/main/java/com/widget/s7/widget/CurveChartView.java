package com.widget.s7.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CurveChartView extends View {

    private boolean haveFocuse = false;

    private int margin = 40;

    private int model = MODEL_MONTH;
    public static final int MODEL_YEAR = 1;
    public static final int MODEL_MONTH = 2;

    private int days = 32;

    private int viewWidth, viewHeigth;
    private float averageWidth, averageHeigth;
    Paint textPaint, lienPaint, bluePaint, whitePaint, garyPaint, coordinatePaint;

    private Path curvePath;

    private List<Coordinate> coordinates;

    private Coordinate coordinate;

    private boolean isFlag = false;

    public CurveChartView(Context context) {
        super(context);
        init();
    }

    public CurveChartView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CurveChartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        textPaint = new Paint();
        textPaint.setColor(Color.parseColor("#999999"));
        textPaint.setAntiAlias(true);
        textPaint.setTextSize(28);
        textPaint.setStrokeWidth(2f);

        lienPaint = new Paint();
        lienPaint.setColor(Color.parseColor("#999999"));
        lienPaint.setAntiAlias(true);
        lienPaint.setStrokeWidth(4f);

        bluePaint = new Paint();
        bluePaint.setColor(Color.parseColor("#2E88CE"));
        bluePaint.setAntiAlias(true);
        bluePaint.setStyle(Paint.Style.STROKE);
        bluePaint.setStrokeWidth(6f);

        whitePaint = new Paint();
        whitePaint.setColor(Color.parseColor("#ffffff"));
        whitePaint.setStrokeWidth(2.5f);

        garyPaint = new Paint();
        garyPaint.setColor(Color.parseColor("#77333333"));
        garyPaint.setStyle(Paint.Style.FILL);
        garyPaint.setStrokeWidth(2f);

        coordinatePaint = new Paint();
        coordinatePaint.setColor(Color.parseColor("#FFFFFF"));
        coordinatePaint.setAntiAlias(true);
        coordinatePaint.setTextSize(28);
        coordinatePaint.setStrokeWidth(2f);

        curvePath = new Path();

        coordinates = new ArrayList<>();
    }

    public void setMonth(int year, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month - 1);
        days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH) + 1;

    }

    public void setModel(int model) {
        this.model = model;
        initMeasure();
        invalidate();
    }

    private void initMeasure() {
        viewWidth = getWidth() - margin * 3;
        viewHeigth = getHeight() - margin * 3;
        averageHeigth = viewHeigth / 11;
        switch (model) {
            case MODEL_YEAR:
                averageWidth = viewWidth / 13;
                break;
            case MODEL_MONTH:
                averageWidth = viewWidth / days;
                break;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawGrayBackground(canvas);
        drawYCoordinateNumber(canvas);
        drawYCoordinateLine(canvas);
        drawXCoordinateText(canvas);
        drawXCoordinateLine(canvas);
        drawCurveChart(canvas);
        if (coordinate != null && isFlag) {
            drawCoordinate(canvas);
        }
    }

    private void drawGrayBackground(Canvas canvas) {
        whitePaint.setStyle(Paint.Style.FILL);
        canvas.drawRect(0, 0, getWidth(), getHeight(), whitePaint);
    }

    private void drawXCoordinateText(Canvas canvas) {
        float x = margin + (margin / 9) * 7;
        float y = margin + (margin / 4) * 3;
        switch (model) {
            case MODEL_YEAR: // 年份
                canvas.drawText("(月)", 13 * averageWidth + x, viewHeigth + y, textPaint);
                for (int i = 0; i < 13; i++) {
                    canvas.drawText(String.valueOf(i), i * averageWidth + x, viewHeigth + y, textPaint);
                }
                break;
            case MODEL_MONTH: // 月份
                canvas.drawText("(天)", days * averageWidth + x, viewHeigth + y, textPaint);
                for (int i = 0; i < days; i++) {
                    if ( i % 2 == 0) {
                        canvas.drawText(String.valueOf(i), i * averageWidth + x, viewHeigth + y, textPaint);
                    }
                }
                break;
        }
    }

    private void drawYCoordinateNumber(Canvas canvas) {
        float y = margin + margin / 3;
        switch (model) {
            case MODEL_YEAR: // 年份
                canvas.drawText("(100单)", margin, margin, textPaint);
                for (int i = 1; i < 11; i++) {
                    canvas.drawText(String.valueOf(i), margin, (float) (11 - i) * averageHeigth + y, textPaint);
                }
                break;
            case MODEL_MONTH: // 月份
                canvas.drawText("(单)", margin, margin, textPaint);
                for (int i = 1; i < 11; i++) {
                    canvas.drawText(String.valueOf(i * 10), margin / 4 * 3, (float) (11 - i) * averageHeigth + y, textPaint);
                }
                break;
        }
    }

    private void drawXCoordinateLine(Canvas canvas) {
        float x = margin * 2;
        float y = margin + margin / 5;
        switch (model) {
            case MODEL_YEAR: // 年份
                canvas.drawLine(x, x, x, viewHeigth + margin, lienPaint);
                for (int i = 1; i < 13; i++) {
                    canvas.drawLine(x + averageWidth * i, averageHeigth * 11 + margin, averageWidth * i + x, viewHeigth + y, lienPaint);
                }
                break;
            case MODEL_MONTH: // 月份
                canvas.drawLine(x, margin, x, viewHeigth + margin, lienPaint);
                for (int i = 1; i < days; i++) {
                    canvas.drawLine(x + averageWidth * i, averageHeigth * 11 + margin, averageWidth * i + x, viewHeigth + y, lienPaint);
                }
                break;
        }
    }

    private void drawYCoordinateLine(Canvas canvas) {
        float x = margin * 2;
//        for (int i = 0; i < 12; i ++) {
//            canvas.drawLine(x, averageHeigth * i + margin, viewWidth + x, averageHeigth * i + margin, lienPaint);
//        }
        canvas.drawLine(x, averageHeigth * 11 + margin, viewWidth + x, averageHeigth * 11 + margin, lienPaint);
    }

    private void drawCurveChart(Canvas canvas) {
        curvePath.reset();
        coordinates.clear();
        float x = margin * 2;
        float y = averageHeigth * 11 + margin;
        curvePath.moveTo(x, y);
        int num = 0;
        switch (model) {
            case MODEL_YEAR:
                num = 13;
                break;
            case MODEL_MONTH:
                num = days;
                break;
        }
//        Log.d("TAG", "共有数据：" + num);
        for (int i = 1; i < num ; i++) {
//            Log.d("TAG", "加载数据数据i：" + i);
            int s = (int)(Math.random() * 10+1);
//            Log.d("TAG", "加载数据数据s：" + s);
            getCoordinate(i, s);
        }
        canvas.drawPath(curvePath, bluePaint);
    }

    private void getCoordinate(int i, int s) {
        float x = margin * 2;
        float y = averageHeigth * 11 + margin;
        if (s == 0) {
            s = 1;
        }
        x = x + i * averageWidth;
        y = y - s * averageHeigth;
        curvePath.lineTo(x, y);
        Coordinate coordinate = new Coordinate();
        coordinate.setX(x);
        coordinate.setY(y);
        coordinates.add(coordinate);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (coordinates.size() > 0) {
                for (Coordinate coordinate : coordinates) {
                    if ((coordinate.getX() - 20 <= event.getX() && coordinate.getX() + 20 >= event.getX())
                            && (coordinate.getY() - 20 <= event.getY() && coordinate.getY() + 20 >= event.getY())) {
                        isFlag = true;
                        this.coordinate = coordinate;
                        invalidate();
                    }
                }
            }
        }
        return true;
    }

    private void drawCoordinate(Canvas canvas) {
        isFlag = false;
        Rect rect = new Rect();
        float y = averageHeigth * 11 + margin;
        float s = (y - coordinate.getY()) / averageHeigth;
        String pers = String.valueOf(s);
        coordinatePaint.getTextBounds(pers, 0, pers.length(), rect);// 确定文字的宽度

        canvas.drawRect(coordinate.getX() - rect.width() / 2  - 20, coordinate.getY() - rect.height() - 40,
                coordinate.getX() + rect.width() / 2  + 20, coordinate.getY(), garyPaint);

        canvas.drawText(pers, coordinate.getX() - rect.width() / 2, coordinate.getY() - rect.height(), coordinatePaint); // 灰色背景
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        if (hasWindowFocus) {
            haveFocuse = true;
            initMeasure();
            setModel(this.model);
        }
        super.onWindowFocusChanged(hasWindowFocus);
    }

    class Coordinate {

        private float x;
        private float y;

        public float getX() {
            return x;
        }

        public void setX(float x) {
            this.x = x;
        }

        public float getY() {
            return y;
        }

        public void setY(float y) {
            this.y = y;
        }
    }
}
