package com.widget.s7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.inputmethod.EditorInfo;

import com.widget.s7.R;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;

public class PasswordEditText extends AppCompatEditText {

    private Paint passwordPaint = new Paint(ANTI_ALIAS_FLAG);

    private int textLength;

    private int pwdLength = 6;
    private int pwdColor = Color.parseColor("#222222");
    private float pwdSize = 3;
    private float spacingWidth = 3;

    public PasswordEditText(Context context) {
        this(context, null);
    }

    public PasswordEditText(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PasswordEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initAttributeSet(context, attrs);
        init();
    }

    private void initAttributeSet(Context context, AttributeSet attrs) {
        DisplayMetrics dm = getResources().getDisplayMetrics();
        pwdSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, pwdSize, dm);
        pwdLength = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, pwdLength, dm);
        spacingWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spacingWidth, dm);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.PasswordEditText, 0, 0);
        pwdLength = a.getInt(R.styleable.PasswordEditText_passwordLength, pwdLength);
        pwdColor = a.getColor(R.styleable.PasswordEditText_passwordColor, pwdColor);
        pwdSize = a.getDimension(R.styleable.PasswordEditText_passwordSize, pwdSize);
        spacingWidth = a.getDimension(R.styleable.PasswordEditText_spacingWidth, spacingWidth);
        a.recycle();
    }

    private void init() {
        setBackground(null);
        setInputType(EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);

        setEditLength();

        passwordPaint.setStrokeWidth(pwdSize);
        passwordPaint.setStyle(Paint.Style.FILL);
        passwordPaint.setColor(pwdColor);
    }

    private void setEditLength() {
        setMaxEms(this.pwdLength);
        setFilters(new InputFilter[]{new InputFilter.LengthFilter(this.pwdLength)});
    }

    public int getPwdLength() {
        return pwdLength;
    }

    public void setPwdLength(int pwdLength) {
        this.pwdLength = pwdLength;
        setEditLength();
        invalidate();
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        this.textLength = text.toString().length();
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
//        pwdSize = getHeight() / 15;
//        if (pwdSize == 0) {
//            pwdSize = 5;
//        }

        setTextColor(0x00000000);
        setTextSize(0);

        drawPwd(canvas);
    }


    private void drawPwd(Canvas canvas) {
        for(int i = 0; i < textLength; i++) {
            float cx = getPaddingLeft() + pwdSize / 2 + spacingWidth * (i + 1) * 2 + pwdSize * i;
            canvas.drawCircle(cx, getHeight() / 2, pwdSize, passwordPaint);
        }
    }


}
