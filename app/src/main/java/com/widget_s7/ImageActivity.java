package com.widget_s7;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.widget.s7.widget.RoundImageView;

public class ImageActivity extends AppCompatActivity {

    private RoundImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        image = findViewById(R.id.image_1);

        MyApp.getInstance().setImage("http://img2.3lian.com/2014/f2/37/d/39.jpg", image);


    }
}
