# s7_widget

#### 项目介绍
   一个自定义View的基础库
   
#### 使用说明
   介绍文档
      https://jitpack.io/com/gitee/sy7/s7_widget/V1.4.3/javadoc/
          
   获取版本号
      https://jitpack.io/#com.gitee.sy7/s7_widget
         
    在build.gradle中添加依赖库
        allprojects {
   		  repositories {
   			  maven { url 'https://jitpack.io' }
   		  }
   	  } 
         dependencies {
   		  implementation 'com.gitee.sy7:s7_widget:V1.4.3'
   	  }

#### 内容说明
    PasswordEdit (支付密码输入框);
    NumberKeyboardHelper (自定义数字密码键盘);
    WheelPicker (滚轮选择器);
    PasswordText (密码TextView);
    SwipeMenuLayout (带侧滑的Layout布局（用于ListView的Item）
      isIos 是否是IOS阻塞式交互，默认是打开的。
      isSwipeEnable 是否开启右滑菜单，默认打开。（某些场景，复用item，没有编辑权限的用户不能右滑）
      isLeftSwipe 支持左滑右滑）;
    CircleImageView (圆形ImageView);
    RoundImageView (可显示为圆形、方形; 可设置 圆角、边框、蒙板);
    ProcessImageView (带进度的ImageView);
    GradualTabScrollView (标题栏渐变/菜单顶部固定 ScrollView
        isShade 是否开启渐变
        isFixedTab 是否开启顶部固定菜单栏);
    ProgressWebView (带进度的WebView);
    TimeCountUtil (button倒计时);
    TitleBar (标题栏);
    BaseListAdapter (ListView、GridView 等的适配器)
    FlowLayoutManager (RecyclerView 流式布局LayoutManager (自动换行));
    DividerItemDecoration (RecyclerView分割线颜色);
    LoadMoreHelper (RecyclerView上下拉取功能工具类);
    BaseRecyclerAdapter (RecyclerView上下拉取功能适配器);
    RecyclerHelper (RecyclerView普通工具类)；
    BaseRecyclerViewAdapter (RecyclerView普通适配器);
    ConvenientBanner (带指示器的自动翻页Banner);
    TabLayoutManager (TabLayout下滑线长度控制工具);
    PositionScrollView (PositionScroll 滚动到指定的孙View，加载下一个数据，返回当前滑入屏幕的View的位置);
    AdmixEdit (图文混合输入框);
    RoundImageView (可显示为圆形、方形; 可设置 圆角、边框)；


