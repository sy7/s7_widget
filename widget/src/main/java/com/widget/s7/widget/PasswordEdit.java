package com.widget.s7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v7.widget.AppCompatEditText;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.inputmethod.EditorInfo;


import com.widget.s7.R;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;

public class PasswordEdit extends AppCompatEditText {

    private float itemWidth;

    private int borderColor = Color.parseColor("#a5a5a5");
    private float borderWidth = 3;
    private float borderRadius = 0;

    private int divisionLineColor = Color.parseColor("#a5a5a5");
    private float divisionLineWidth = 1;

    private int pwdLength = 6;
    private int pwdColor = Color.parseColor("#222222");
    private float pwdSize = 3;

    private Paint passwordPaint = new Paint(ANTI_ALIAS_FLAG);
    private Paint divisionLinePaint = new Paint(ANTI_ALIAS_FLAG);
    private Paint borderPaint = new Paint(ANTI_ALIAS_FLAG);

    private int textLength;

    public PasswordEdit(Context context) {
        this(context, null);
    }

    public PasswordEdit(Context context, AttributeSet attrs) {
        super(context, attrs);

        initAttributeSet(context, attrs);

        initPaint();

        setBackground(null);
        setInputType(EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);
        setCursorVisible(false);

        setEditLength();
    }

    private void initAttributeSet(Context context, AttributeSet attrs) {
        DisplayMetrics dm = getResources().getDisplayMetrics();
        borderWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, borderWidth, dm);
        borderRadius = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, borderRadius, dm);
        divisionLineWidth  = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, divisionLineWidth, dm);
        pwdLength = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, pwdLength, dm);
        pwdSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, pwdSize, dm);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.PasswordEdit, 0, 0);
        borderColor = a.getColor(R.styleable.PasswordEdit_borderColor, borderColor);
        borderWidth = a.getDimension(R.styleable.PasswordEdit_borderWidth, borderWidth);
        borderRadius = a.getDimension(R.styleable.PasswordEdit_borderRadius, borderRadius);
        divisionLineColor = a.getColor(R.styleable.PasswordEdit_divisionLineColor, divisionLineColor);
        divisionLineWidth = a.getDimension(R.styleable.PasswordEdit_divisionLineWidth, divisionLineWidth);
        pwdLength = a.getInt(R.styleable.PasswordEdit_pwdLength, pwdLength);
        pwdColor = a.getColor(R.styleable.PasswordEdit_pwdColor, pwdColor);
        pwdSize = a.getDimension(R.styleable.PasswordEdit_pwdSize, pwdSize);
        a.recycle();
    }

    private void setEditLength() {
        setMaxEms(this.pwdLength);
        setFilters(new InputFilter[]{new InputFilter.LengthFilter(this.pwdLength)});
    }

    private void initPaint() {
        borderPaint.setAntiAlias(true);
        divisionLinePaint.setAntiAlias(true);
        passwordPaint.setAntiAlias(true);
        borderPaint.setStrokeWidth(borderWidth);
        borderPaint.setColor(borderColor);

        borderPaint.setStyle(Paint.Style.STROKE);
        divisionLinePaint.setStrokeWidth(divisionLineWidth);
        divisionLinePaint.setColor(divisionLineColor);
        passwordPaint.setStrokeWidth(pwdSize);
        passwordPaint.setStyle(Paint.Style.FILL);
        passwordPaint.setColor(pwdColor);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        float passwordWidth = getWidth() - (pwdLength - 1) * divisionLineWidth - borderWidth * 2;
        itemWidth = passwordWidth / pwdLength;

        drawBorder(canvas);
        drawDivision(canvas);
        drawPwd(canvas);
    }

    private void drawBorder(Canvas canvas) {
        RectF rect = new RectF(borderWidth, borderWidth, getWidth() - borderWidth,
                getHeight()  - borderWidth);
        canvas.drawRoundRect(rect, borderRadius, borderRadius, borderPaint);
    }

    private void drawDivision(Canvas canvas) {
        for (int i = 1; i < pwdLength; i++) {
            float x = borderWidth + itemWidth * i + divisionLineWidth * i;
            canvas.drawLine(x, borderWidth, x, getHeight() - borderWidth, divisionLinePaint);
        }
    }

    private void drawPwd(Canvas canvas) {
        for(int i = 0; i < textLength; i++) {
            float cx = borderWidth + itemWidth * i + divisionLineWidth * i + itemWidth / 2 ;
            canvas.drawCircle(cx, getHeight() / 2, pwdSize, passwordPaint);
        }
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        this.textLength = text.toString().length();
        invalidate();
    }

    public int getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(int borderColor) {
        this.borderColor = borderColor;
        borderPaint.setColor(borderColor);
        invalidate();
    }

    public float getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(float borderWidth) {
        this.borderWidth = borderWidth;
        borderPaint.setStrokeWidth(borderWidth);
        invalidate();
    }

    public float getBorderRadius() {
        return borderRadius;
    }

    public void setBorderRadius(float borderRadius) {
        this.borderRadius = borderRadius;
        invalidate();
    }

    public int getDivisionLineColor() {
        return divisionLineColor;
    }

    public void setDivisionLineColor(int divisionLineColor) {
        this.divisionLineColor = divisionLineColor;
        divisionLinePaint.setColor(divisionLineColor);
        invalidate();
    }

    public float getDivisionLineWidth() {
        return divisionLineWidth;
    }

    public void setDivisionLineWidth(float divisionLineWidth) {
        this.divisionLineWidth = divisionLineWidth;
        divisionLinePaint.setStrokeWidth(divisionLineWidth);
        invalidate();
    }

    public int getPwdLength() {
        return pwdLength;
    }

    public void setPwdLength(int PwdLength) {
        this.pwdLength = PwdLength;
        setEditLength();
        invalidate();
    }

    public int getPwdColor() {
        return pwdColor;
    }

    public void setPwdColor(int TextColor) {
        this.pwdColor = TextColor;
        passwordPaint.setColor(TextColor);
        invalidate();
    }

    public float getPwdSize() {
        return pwdSize;
    }

    public void setPwdSize(float pwdSize) {
        this.pwdSize = pwdSize;
        passwordPaint.setStrokeWidth(pwdSize);
        invalidate();
    }
}