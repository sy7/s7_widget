package com.widget_s7;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class PickerActivity extends AppCompatActivity implements View.OnClickListener {

    private StringWheelPicker mPicker;

    private List<String> datas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picker);
        mPicker = findViewById(R.id.picker);
        findViewById(R.id.btn).setOnClickListener(this);
        datas = new ArrayList<>();
    }


    @Override
    public void onClick(View v) {
        datas.clear();
        for (int i = 0; i<10; i++){
            datas.add("第" + i + "条数据");
        }
        mPicker.setData(datas, 3);
    }
}
