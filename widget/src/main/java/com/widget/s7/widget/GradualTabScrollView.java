package com.widget.s7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.widget.s7.R;
import com.widget.s7.listener.OnTranslucentListener;


public class GradualTabScrollView extends ScrollView {

    private int mHeaderHeight = 0;

    private int mFooterHeight = 0;

    private ViewGroup chidView;

    private View headerView;

    private View tabView;

    private View footerView;

    private View titleBar;

    private boolean isSteep = false;

    private boolean isTitleHeight = false;

    private float stateBarHeight = 0;

    private boolean isShade = false;

    private boolean isFixedTab = false;

    private boolean isIntercept = false;

    private OnTranslucentListener mListener;


    public GradualTabScrollView(Context context) {
        this(context, null);
    }

    public GradualTabScrollView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GradualTabScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public GradualTabScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.GradualTabScrollView, defStyleAttr, defStyleRes);
        setShade(a.getBoolean(R.styleable.GradualTabScrollView_isShade, false));
        setFixedTab(a.getBoolean(R.styleable.GradualTabScrollView_isFixedTab, false));
        setShade(a.getBoolean(R.styleable.GradualTabScrollView_isSteep, false));
        setFixedTab(a.getBoolean(R.styleable.GradualTabScrollView_isTitleHeight, false));
        a.recycle();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        chidView = (ViewGroup) getChildAt(0);
        if (chidView == null) {
            return;
        }

        if (isShade() && chidView.getChildCount() > 0 ) {
            headerView = chidView.getChildAt(0);

            if (isSteep) {
                mHeaderHeight = (int) (headerView.getHeight() - stateBarHeight);
            } else {
                mHeaderHeight = headerView.getHeight();
            }
        }

        if (isFixedTab() && chidView.getChildCount() == 3) {
            headerView = chidView.getChildAt(0);
            tabView = chidView.getChildAt(1);
            footerView = chidView.getChildAt(2);

            if (isSteep) {
                mHeaderHeight = (int) (headerView.getHeight() - stateBarHeight);
            } else {
                mHeaderHeight = headerView.getHeight();
            }

            if (isTitleHeight) {
                mFooterHeight = (int) (getHeight() - tabView.getMeasuredHeight() - stateBarHeight + titleBar.getMeasuredHeight());
            } else {
                mFooterHeight = (int) (getHeight() - tabView.getMeasuredHeight() - stateBarHeight);
            }

            footerView.setLayoutParams(new LinearLayout.LayoutParams(getWidth(), mFooterHeight));
        }

        if (headerView != null) {
            headerView.setFocusable(true);
            headerView.setFocusableInTouchMode(true);
            headerView.requestFocus();
        }
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        initGradualScroll(l, t, oldl, oldt);
        initFixedTab(l, t, oldl, oldt);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return isIntercept ? false : super.onInterceptTouchEvent(ev);
    }

    private void initFixedTab(int l, int t, int oldl, int oldt) {
        if (isFixedTab()) {
            if (t >= mHeaderHeight) {
                isIntercept = true;
            } else {
                isIntercept = false;
            }
        }
    }

    private boolean flag = true;

    private void initGradualScroll(int l, int t, int oldl, int oldt) {

        if (titleBar != null) {
            if (flag) {
                flag = false;
                mHeaderHeight = mHeaderHeight - titleBar.getHeight();
            }
        }

        if (isShade()) {
            if (t <= 0) {
                if (mListener != null) {
                    mListener.onTranslucent(0);
                }
            } else if (t > 0 && t < mHeaderHeight) {
                float scale = (float) t / mHeaderHeight;
                float alpha = (255 * scale);
                if (mListener != null) {
                    mListener.onTranslucent(alpha);
                }
            } else {
                if (mListener != null) {
                    mListener.onTranslucent(255);
                }
            }
        }
    }

    public boolean isShade() {
        return isShade;
    }

    public void setShade(boolean shade) {
        this.isShade = shade;
    }

    public boolean isFixedTab() {
        return isFixedTab;
    }

    public void setFixedTab(boolean fixedTab) {
        isFixedTab = fixedTab;
    }

    public void setSteep(boolean steep) {
        isSteep = steep;
    }

    public void setTitleHeight(boolean titleHeight) {
        isTitleHeight = titleHeight;
    }

    public View getTitleBar() {
        return titleBar;
    }

    public void setTitleBar(View titleBar) {
        this.titleBar = titleBar;
    }

    public float getStateBarHeight() {
        return stateBarHeight;
    }

    public void setStateBarHeight(float stateBarHeight) {
        this.stateBarHeight = stateBarHeight;
    }

    public void setTranslucentListener(OnTranslucentListener mListener) {
        this.mListener = mListener;
    }

}
