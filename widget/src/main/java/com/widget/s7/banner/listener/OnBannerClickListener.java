package com.widget.s7.banner.listener;

public interface OnBannerClickListener<T> {
    void onBannerClick(T data, int position);
}
