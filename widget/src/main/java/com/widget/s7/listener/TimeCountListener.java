package com.widget.s7.listener;

public interface TimeCountListener {

    void onStopwatch(long s);

    void onEnd();
}
