package com.widget.s7.listener;

public interface OnTranslucentListener {
    void onTranslucent(float alpha);
}
