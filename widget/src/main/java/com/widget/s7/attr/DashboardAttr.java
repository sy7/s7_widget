package com.widget.s7.attr;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;

import com.widget.s7.R;
import com.widget.s7.utils.PxUtil;

/**
 * Created by anderson on 2016/6/5.
 */
public class DashboardAttr {

    private boolean isDragging;
    private boolean isTrunc;
    private float mStart;
    private float mEnd;
    private int startColor;
    private int endColor;
    private int progressWidth;
    private int duringArc;
    private int indicatorColor;
    private int mCount;
    private int mCountGroup;
    private int mCountRule;
    private String mUnit;
    private int backgroundColor;
    private int centerCircleColor;
    private int textColor;
    private int textSize;

    public DashboardAttr(Context context, AttributeSet attrs, int defStyleAttr) {
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.DashBoard, defStyleAttr, 0);

        backgroundColor = ta.getColor(R.styleable.DashBoard_dbBackground, 0);
        centerCircleColor = ta.getColor(R.styleable.DashBoard_dbCenterCircle, 0xFF00C1F8);
        isDragging = ta.getBoolean(R.styleable.DashBoard_dbDragging, false);
        isTrunc = ta.getBoolean(R.styleable.DashBoard_dbTrunc, false);
        mStart = ta.getInt(R.styleable.DashBoard_dbStart, 0);
        mEnd = ta.getInt(R.styleable.DashBoard_dbEnd, 240);
        startColor = ta.getColor(R.styleable.DashBoard_dbStartColor, 0xFF08D2F2);
        endColor = ta.getColor(R.styleable.DashBoard_dbEndColor, 0xFFDD0139);
        mCount = ta.getInt(R.styleable.DashBoard_dbCount, 12);
        mCountGroup = ta.getInt(R.styleable.DashBoard_dbCountGroup, 5);
        mCountRule = ta.getInt(R.styleable.DashBoard_dbCountRule, 0);

        duringArc = ta.getInt(R.styleable.DashBoard_dbDuringArc, 240);
        progressWidth = ta.getInt(R.styleable.DashBoard_dbProgressWidth, 15);
        indicatorColor = ta.getColor(R.styleable.DashBoard_dbIndicator, 0xFFFFFFFF);

        mUnit = ta.getString(R.styleable.DashBoard_dbUnit);
        textColor = ta.getColor(R.styleable.DashBoard_dbTextColor, 0xFFFFFFFF);
        textSize = ta.getDimensionPixelSize(PxUtil.spToPx(R.styleable.DashBoard_dbTextSize, context), 40);
        ta.recycle();
    }

    public boolean isDragging() {
        return isDragging;
    }

    public boolean isTrunc() {
        return isTrunc;
    }

    public float getStart() {
        return mStart;
    }

    public float getEnd() {
        return mEnd;
    }

    public int getStartColor() {
        return startColor;
    }

    public int getEndColor() {
        return endColor;
    }

    public int getIndicatorColor() {
        return indicatorColor;
    }

    public int getProgressWidth() {
        return progressWidth;
    }

    public int getDuringArc() {
        return duringArc;
    }

    public int getCount() {
        return mCount;
    }

    public int getCountGroup() {
        return mCountGroup;
    }

    public int getCountRule() {
        return mCountRule;
    }

    public String getUnit() {
        return mUnit;
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public int getCenterCircleColor() {
        return centerCircleColor;
    }

    public int getTextColor() {
        return textColor;
    }

    public int getTextSize() {
        return textSize;
    }
}
