package com.widget.s7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.view.ViewGroup;

import com.widget.s7.listener.OnPositionScrollListener;

import java.util.ArrayList;
import java.util.List;


public class PositionScrollView extends NestedScrollView {

    private ViewGroup chidView;

    private boolean isFlag = false;

    private int position = 0;

    public PositionScrollView(@NonNull Context context) {
        super(context);
    }

    public PositionScrollView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public PositionScrollView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        chidView = (ViewGroup) getChildAt(0);
    }

    public void setScrollTo(int position) {
        this.position = position;
        isFlag = true;
        int top = 0;
        if (chidView != null && chidView.getChildCount() >= position) {
            for (int i = 0; i < position; i++) {
                top = top + chidView.getChildAt(i).getHeight();
            }
        }
        scrollTo(0, top);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (isFlag) {
            isFlag = false;
            return;
        }

        if (chidView != null && chidView.getHeight() - getHeight() == getScrollY()) {
            if (listener != null) {
                listener.onPositionLoadData(this);
            }
        }

        initScroll(t, oldt);
    }

    private void initScroll(int t, int oldt) {
        List<Integer> index = new ArrayList<>();
        if (chidView != null && chidView.getChildCount() > 0) {
            Rect scrollBounds = new Rect();
            getHitRect(scrollBounds);
            for (int i = 0; i < chidView.getChildCount(); i++) {
                // 滑入屏幕
                if (chidView.getChildAt(i).getLocalVisibleRect(scrollBounds)) {
                    index.add(i);
                }
            }
            if (listener != null) {
                if (oldt > t && oldt - t > 40) {// 向下
                    if (position != getMin(index, chidView.getChildCount())) {
                        position = getMin(index, chidView.getChildCount());
                        listener.onPositionScroll(this, position);
                    }
                } else if (oldt < t && t - oldt > 40) {// 向上
                    if (position != getMax(index)) {
                        position = getMax(index);
                        listener.onPositionScroll(this, position);
                    }
                }
            }
        }
    }

    private int getMin(List<Integer> indexs, int max) {
        int index = max;
        for (int i : indexs) {
            if (i <= index) {
                index = i;
            }
        }
        return index;
    }

    private int getMax(List<Integer> indexs) {
        int index = 0;
        for (int i : indexs) {
            if (i >= index) {
                index = i;
            }
        }
        return index;
    }

    private OnPositionScrollListener listener;

    public void setPositionScrollListener(OnPositionScrollListener listener) {
        this.listener = listener;
    }

}

