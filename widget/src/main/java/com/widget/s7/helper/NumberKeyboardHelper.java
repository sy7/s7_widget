package com.widget.s7.helper;

import android.content.Context;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.text.Editable;
import android.widget.EditText;
import android.widget.TextView;

import com.widget.s7.R;
import com.widget.s7.listener.OnKeyboardListener;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class NumberKeyboardHelper implements KeyboardView.OnKeyboardActionListener {

    public static final int KEYCODE_EMPTY = -10;

    private Context mContext;
    private KeyboardView mKeyboard;
    private EditText mEdit;
    private OnKeyboardListener mListener;

    private boolean hasInit = false;

    public NumberKeyboardHelper(Context mContext) {
        this.mContext = mContext;
    }

    public NumberKeyboardHelper setKeyboardView(KeyboardView mKeyboard) {
        this.mKeyboard = mKeyboard;
        return this;
    }

    public NumberKeyboardHelper setEditText(EditText mEdit) {
        this.mEdit = mEdit;
        return this;
    }

    public NumberKeyboardHelper setOnKeyboardListener(OnKeyboardListener mListener) {
        this.mListener = mListener;
        return this;
    }


    public void init() {
        if (!hasInit) {
            if (mKeyboard != null) {
                Keyboard keyboard = new Keyboard(mContext, R.xml.keyboard_number);
                mKeyboard.setKeyboard(keyboard);
                mKeyboard.setEnabled(true);
                mKeyboard.setPreviewEnabled(false);
                mKeyboard.setOnKeyboardActionListener(this);
            }
            hasInit = true;
        }
    }
    public void init(int xml) {
        if (!hasInit) {
            if (mKeyboard != null) {
                Keyboard keyboard = new Keyboard(mContext, xml);
                mKeyboard.setKeyboard(keyboard);
                mKeyboard.setEnabled(true);
                mKeyboard.setPreviewEnabled(false);
                mKeyboard.setOnKeyboardActionListener(this);
            }
            hasInit = true;
        }
    }


    private final List<Character> keyCodes = Arrays.asList('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');

    public void shuffleKeyboard() {
        Keyboard keyboard = mKeyboard.getKeyboard();
        if (keyboard != null && keyboard.getKeys() != null
                && keyboard.getKeys().size() > 0) {
            Collections.shuffle(keyCodes);
            List<Keyboard.Key> keys = mKeyboard.getKeyboard().getKeys();
            int index = 0;
            for (Keyboard.Key key : keys) {
                if (key.codes[0] != KEYCODE_EMPTY
                        && key.codes[0] != Keyboard.KEYCODE_DELETE) {
                    char code = keyCodes.get(index++);
                    key.codes[0] = code;
                    key.label = Character.toString(code);
                }
            }
            mKeyboard.setKeyboard(keyboard);
        }
    }

    @Override
    public void onPress(int primaryCode) {

    }

    @Override
    public void onRelease(int primaryCode) {

    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {
        if (mEdit != null) {
            Editable editable = mEdit.getText();
            int start = mEdit.getSelectionStart();
            if (primaryCode == Keyboard.KEYCODE_DELETE) {
                if (editable != null && editable.length() > 0) {
                    if (start > 0) {
                        editable.delete(start - 1, start);
                    }
                }
            } else if (primaryCode > 0) {
                editable.insert(start, Character.toString((char) primaryCode));
            } else {
                if (mListener != null) {
                    mListener.onKeyEvent(primaryCode);
                }
            }
        }
    }

    public Editable getText(TextView textView) {
        CharSequence text = textView.getText();
        if (text == null) {
            return null;
        }
        if (text instanceof Editable) {
            return (Editable) textView.getText();
        }
        textView.setText(text, TextView.BufferType.EDITABLE);
        return (Editable) textView.getText();
    }

    @Override
    public void onText(CharSequence text) {

    }

    @Override
    public void swipeLeft() {

    }

    @Override
    public void swipeRight() {

    }

    @Override
    public void swipeDown() {

    }

    @Override
    public void swipeUp() {

    }
}
