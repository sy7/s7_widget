package com.widget.s7.listener;

import android.support.v4.widget.NestedScrollView;

public interface OnPositionScrollListener {

    void onPositionLoadData(NestedScrollView view);

    void onPositionScroll(NestedScrollView view, int position);
}
