package com.widget.s7.banner.holder;

import android.view.View;

public interface CBViewHolderCreator {
	int getLayoutId();
	Holder createHolder(View itemView);
}
