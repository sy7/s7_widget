package com.widget.s7.listener;

import android.view.View;

public interface OnTitleListener {
    void onTitle(View view);
}
