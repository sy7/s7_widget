package com.widget.s7.admix.holder;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;

import java.util.HashMap;
import java.util.Map;

public class BaseHolder extends ViewHolder {
    private Map<Integer, View> mViewMap;

    public BaseHolder(View itemView) {
        super(itemView);
        mViewMap = new HashMap<>();
    }

    public <T extends View> T getItemView() {
        return (T) itemView;
    }

    public <T extends View> T getView(int id) {
        View view = mViewMap.get(id);
        if (view == null) {
            view = itemView.findViewById(id);
            mViewMap.put(id, view);
        }
        return (T) view;
    }

    private int realPosition;

    public void setRealPosition(int realPosition) {
        this.realPosition = realPosition;
    }

    public int getRealPosition() {
        return realPosition;
    }

    public void setLayoutParams(int id, int width, int height) {
        getView(id).getLayoutParams().width = width;
        getView(id).getLayoutParams().height = height;
    }

    public void setTag(int id, Object tag) {
        getView(id).setTag(tag);
    }

}
