package com.widget_s7.adapter;

import android.view.View;
import android.widget.ImageView;

import com.widget.s7.banner.holder.CBViewHolderCreator;
import com.widget.s7.banner.holder.Holder;
import com.widget_s7.MyApp;
import com.widget_s7.R;

public class BannerAdapter  implements CBViewHolderCreator {

    @Override
    public Holder createHolder(View itemView) {
        return new LocalImageHolderView(itemView);
    }

    @Override
    public int getLayoutId() {
        return R.layout.banner_item;
    }

    public static class LocalImageHolderView extends Holder<String> {

        private ImageView mImage;

        public LocalImageHolderView(View itemView) {
            super(itemView);
        }

        @Override
        protected void initView(View itemView) {
            mImage = itemView.findViewById(R.id.banner_image);
        }

        @Override
        public void updateUI(String data) {
            MyApp.getInstance().setImage(data, mImage);
        }
    }
}
