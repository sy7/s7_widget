package com.widget.s7.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.util.AttributeSet;

import com.widget.s7.R;

public class ProcessImageView extends AppCompatImageView {

    private Paint mPaint;
    Context context = null;
    int progress = 0;

    private boolean isDefault = true;
    private String hint;
    private Bitmap bitmap;
    private boolean isFinished = false;

    public ProcessImageView(Context context) {
        super(context);
    }

    public ProcessImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProcessImageView(Context context, AttributeSet attrs,
                            int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ProcessImageView, defStyleAttr, 0);
        hint = a.getString(R.styleable.ProcessImageView_text);

        mPaint = new Paint();
        bitmap = getBitmap();

        a.recycle();
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mPaint.setAntiAlias(true); // 消除锯齿
        mPaint.setStyle(Paint.Style.FILL);

        if (!isDefault) {
            if (isFinished)
                return;

            mPaint.setColor(Color.parseColor("#70000000"));// 半透明
            canvas.drawRect(0, 0, getWidth(), getHeight()- getHeight() * progress
                    / 100, mPaint);

            mPaint.setColor(Color.parseColor("#00000000"));// 全透明
            canvas.drawRect(0, getHeight() - getHeight() * progress / 100,
                    getWidth(), getHeight(), mPaint);

            mPaint.setTextSize(35);
            mPaint.setColor(Color.parseColor("#FFFFFF"));
            mPaint.setStrokeWidth(2);

            Rect rect = new Rect();
            String pers = progress + "%";
            mPaint.getTextBounds(pers, 0, pers.length(), rect);
            canvas.drawText(pers, getWidth() / 2 - rect.width() / 2,
                    getHeight() / 2 + rect.height() / 2, mPaint);
        } else {
            // 白色背景
            mPaint.setColor(Color.parseColor("#EEEEEE"));
            canvas.drawRect(0, 0, getWidth(), getHeight(), mPaint);

            canvas.drawBitmap(bitmap, getWidth() / 2 - bitmap.getWidth() / 2,
                    getHeight() / 2 - bitmap.getHeight() / 2, mPaint);

            if (!TextUtils.isEmpty(hint)) {
                mPaint.setTextSize(35);
                mPaint.setColor(Color.parseColor("#CCCCCC"));
                mPaint.setStrokeWidth(2);

                Rect rect = new Rect();
                mPaint.getTextBounds(hint, 0, hint.length(), rect);
                canvas.drawText(hint, getWidth() / 2 - rect.width() / 2,
                        getHeight() / 2 + bitmap.getHeight() + 10, mPaint);
            }
        }

    }

    public Bitmap getBitmap() {
        Resources r = this.getContext().getResources();
        Bitmap bmp = BitmapFactory.decodeResource(r, R.mipmap.tools_btn_add);
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        int newWidth = 90;
        int newHeight = 90;
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        return Bitmap.createBitmap(bmp, 0, 0, width, height, matrix, true);
    }

    public void setIsDefault(boolean isDefault) {
        this.isDefault = isDefault;
        postInvalidate();
    }

    public void setHint(String hint) {
        this.hint = hint;
        postInvalidate();
    }

    public void setProgress(int progress) {
        this.progress = progress;
        postInvalidate();
    }

    public void setFinish() {
        this.isFinished = true;
        postInvalidate();
    }


}
