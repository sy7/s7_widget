package com.widget.s7.utils;

import android.annotation.SuppressLint;
import android.os.CountDownTimer;
import android.widget.Button;

import com.widget.s7.listener.TimeCountListener;

public class TimeCountUtil extends CountDownTimer {

    private Button btn;
    private TimeCountListener mListener;

    public TimeCountUtil(long millisInFuture, Button btn, TimeCountListener mListener) {
        super(millisInFuture * 1000, 1000);
        this.btn = btn;
        this.mListener = mListener;
    }

    @SuppressLint("NewApi")
    @Override
    public void onTick(long millisUntilFinished) {
        btn.setClickable(false);
        if (mListener != null) {
            mListener.onStopwatch(millisUntilFinished / 1000);
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onFinish() {
        btn.setClickable(true);
        if (mListener != null) {
            mListener.onEnd();
        }
    }
}
