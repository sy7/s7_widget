package com.widget.s7.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.support.v7.widget.AppCompatTextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;

public class PasswordText extends AppCompatTextView {

    private int pwdColor = Color.parseColor("#222222");
    private float pwdSize;

    private Paint passwordPaint = new Paint(ANTI_ALIAS_FLAG);

    public PasswordText(Context context) {
        super(context);
        init();
    }

    public PasswordText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public PasswordText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init();
    }

    private void init() {
        passwordPaint.setStrokeWidth(pwdSize);
        passwordPaint.setStyle(Paint.Style.FILL);
        passwordPaint.setColor(pwdColor);
    }

    private String password;

    public void setPassword(String password) {
        this.password = password;
        //重新画控件
        this.invalidate();
    }

    public String getPassword() {
        return password;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        pwdSize = getHeight() / 15;
        if (pwdSize == 0) {
            pwdSize = 5;
        }
        if (TextUtils.isEmpty(password))
            return;
        drawPwd(canvas);
    }

    private void drawPwd(Canvas canvas) {
        for(int i = 0; i < password.length(); i++) {
            float cx = getPaddingLeft() + pwdSize / 2 + pwdSize * (i + 1) * 2 + pwdSize * i;
            canvas.drawCircle(cx, getHeight() / 2, pwdSize, passwordPaint);
        }
    }


}
