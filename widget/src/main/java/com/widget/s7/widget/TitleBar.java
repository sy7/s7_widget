package com.widget.s7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.widget.s7.R;
import com.widget.s7.listener.OnTitleListener;

public class TitleBar extends RelativeLayout implements View.OnClickListener {

    private TextView mLeft, mTitle, mRight;

    private String leftText, titleText, rightText;

    private float leftSize = 10, titleSize = 13, rightSize = 10;

    private int leftPadding = 2, rightPadding = 2;
    private int leftColor = Color.BLACK, titleColor = Color.BLACK, rightColor = Color.BLACK;

    private Drawable leftIco, titleIco, rightIco;

    private int leftIcoPadding = 5, titleIcoPadding = 5, rightIcoPadding = 5;

    private Context context;

    private OnTitleListener mListener;

    public TitleBar(Context context) {
        super(context);
        initViews(context);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews(context);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TitleBar, defStyleAttr, 0);

        setLeftText(a.getString(R.styleable.TitleBar_leftText));
        setLeftColor(a.getColor(R.styleable.TitleBar_leftColor, Color.BLACK));
        setLeftSize(getSize(a.getDimension(R.styleable.TitleBar_leftSize, 10)));
        setLeftPadding((int) getSize(a.getDimension(R.styleable.TitleBar_leftPadding, 2)));
        setLeftIco(a.getDrawable(R.styleable.TitleBar_leftIco));
        setLeftIcoPadding((int) getSize(a.getDimension(R.styleable.TitleBar_leftIcoPadding, 5)));
        setTitleText(a.getString(R.styleable.TitleBar_titleText));
        setTitleColor(a.getColor(R.styleable.TitleBar_titleColor, Color.BLACK));
        setTitleSize(getSize(a.getDimension(R.styleable.TitleBar_titleSize, 13)));
        setTitleIco(a.getDrawable(R.styleable.TitleBar_titleIco));
        setTitleIcoPadding((int) getSize(a.getDimension(R.styleable.TitleBar_titleIcoPadding, 5)));
        setRightText(a.getString(R.styleable.TitleBar_rightText));
        setRightColor(a.getColor(R.styleable.TitleBar_rightColor, Color.BLACK));
        setRightSize(getSize(a.getDimension(R.styleable.TitleBar_rightSize, 10)));
        setRightPadding((int) getSize(a.getDimension(R.styleable.TitleBar_rightPadding, 2)));
        setRightIco(a.getDrawable(R.styleable.TitleBar_rightIco));
        setRightIcoPadding((int) getSize(a.getDimension(R.styleable.TitleBar_rightIcoPadding, 5)));
        a.recycle();

    }

    private void initViews(Context context) {
        LayoutInflater.from(context).inflate(R.layout.bar_title, this);
        this.context = context;
        mLeft = findViewById(R.id.bar_left);
        mTitle = findViewById(R.id.bar_title);
        mRight = findViewById(R.id.bar_right);

        mLeft.setOnClickListener(this);
        mRight.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onTitle(v);
        }
    }

    public TitleBar setListener(OnTitleListener mListener) {
        this.mListener = mListener;
        return this;
    }

    public TextView getLeftView() {
        return mLeft;
    }

    public TextView getTitleView() {
        return mTitle;
    }

    public TextView getRightView() {
        return mRight;
    }

    private float getSize(float size) {
        DisplayMetrics dm = getResources().getDisplayMetrics();
        float value = dm.scaledDensity;
        return size / value;
    }

    public String getLeftText() {
        return leftText;
    }

    public TitleBar setLeftText(String leftText) {
        this.leftText = leftText;
        mLeft.setText(this.leftText);
        return this;
    }

    public String getTitleText() {
        return titleText;
    }

    public TitleBar setTitleText(String titleText) {
        this.titleText = titleText;
        mTitle.setText(this.titleText);
        return this;
    }

    public String getRightText() {
        return rightText;
    }

    public TitleBar setRightText(String rightText) {
        this.rightText = rightText;
        mRight.setText(this.rightText);
        return this;
    }

    public TitleBar setLeftSize(float leftSize) {
        this.leftSize = leftSize;
        mLeft.setTextSize(this.leftSize);
        return this;
    }

    public TitleBar setTitleSize(float titleSize) {
        this.titleSize = titleSize;
        mTitle.setTextSize(this.titleSize);
        return this;
    }

    public TitleBar setRightSize(float rightSize) {
        this.rightSize = rightSize;
        mRight.setTextSize(this.rightSize);
        return this;
    }

    public TitleBar setLeftColor(int leftColor) {
        this.leftColor = leftColor;
        mLeft.setTextColor(this.leftColor);
        return this;
    }

    public TitleBar setTitleColor(int titleColor) {
        this.titleColor = titleColor;
        mTitle.setTextColor(this.titleColor);
        return this;
    }

    public TitleBar setRightColor(int rightColor) {
        this.rightColor = rightColor;
        mRight.setTextColor(this.rightColor);
        return this;
    }

    public TitleBar setLeftIco(Drawable leftIco) {
        this.leftIco = leftIco;
        mLeft.setCompoundDrawablesWithIntrinsicBounds(this.leftIco, null, null, null);
        return this;

    }

    public TitleBar setTitleIco(Drawable titleIco) {
        this.titleIco = titleIco;
        mTitle.setCompoundDrawablesWithIntrinsicBounds(null, this.titleIco, null, null);
        return this;
    }

    public TitleBar setRightIco(Drawable rightIco) {
        this.rightIco = rightIco;
        mRight.setCompoundDrawablesWithIntrinsicBounds(null, null, this.rightIco, null);
        return this;
    }

    public TitleBar setLeftTextResources(int leftText) {
        this.leftText = getResources().getString(leftText);
        mLeft.setText(this.leftText);
        return this;
    }

    public TitleBar setTitleTextResources(int titleText) {
        this.titleText = getResources().getString(titleText);
        mTitle.setText(this.titleText);
        return this;
    }

    public TitleBar setRightTextResources(int rightText) {
        this.rightText = getResources().getString(rightText);
        mRight.setText(this.rightText);
        return this;
    }

    public TitleBar setLeftSizeResources(int leftSize) {
        this.leftSize = getSize(getResources().getDimension(leftSize));
        mLeft.setTextSize(this.leftSize);
        return this;
    }

    public TitleBar setTitleSizeResources(int titleSize) {
        this.titleSize = getSize(getResources().getDimension(titleSize));
        mTitle.setTextSize(this.titleSize);
        return this;
    }

    public TitleBar setRightSizeResources(int rightSize) {
        this.rightSize = getSize(getResources().getDimension(rightSize));
        mRight.setTextSize(this.rightSize);
        return this;
    }

    public TitleBar setLeftColorResources(int leftColor) {
        this.leftColor = getResources().getColor(leftColor);
        mLeft.setTextColor(this.leftColor);
        return this;
    }

    public TitleBar setTitleColorResources(int titleColor) {
        this.titleColor = getResources().getColor(titleColor);
        mTitle.setTextColor(this.titleColor);
        return this;
    }

    public TitleBar setRightColorResources(int rightColor) {
        this.rightColor = getResources().getColor(rightColor);
        mRight.setTextColor(this.rightColor);
        return this;
    }

    public TitleBar setLeftIcoResources(int leftIco) {
        this.leftIco = getResources().getDrawable(leftIco);
        mLeft.setCompoundDrawablesWithIntrinsicBounds(this.leftIco, null, null, null);
        return this;

    }

    public TitleBar setTitleIcoResources(int titleIco) {
        this.titleIco = getResources().getDrawable(titleIco);
        mTitle.setCompoundDrawablesWithIntrinsicBounds(null, this.titleIco, null, null);
        return this;
    }

    public TitleBar setRightIcoResources(int rightIco) {
        this.rightIco = getResources().getDrawable(rightIco);
        mRight.setCompoundDrawablesWithIntrinsicBounds(null, null, this.rightIco, null);
        return this;
    }

    public void setLeftPadding(int leftPadding) {
        this.leftPadding = leftPadding;
        mLeft.setPadding(this.leftPadding, 0, this.leftPadding, 0);
    }

    public void setRightPadding(int rightPadding) {
        this.rightPadding = rightPadding;
        mRight.setPadding(this.rightPadding, 0, this.rightPadding, 0);
    }

    public void setLeftPaddingResources(int leftPadding) {
        this.leftPadding = (int) getSize(getResources().getDimension(leftPadding));
        mLeft.setPadding(this.leftPadding, 0, this.leftPadding, 0);
    }

    public void setRightPaddingResources(int rightPadding) {
        this.rightPadding = (int) getSize(getResources().getDimension(rightPadding));
        mRight.setPadding(this.rightPadding, 0, this.rightPadding, 0);
    }

    public void setLeftIcoPadding(int leftIcoPadding) {
        this.leftIcoPadding = leftIcoPadding;
        mLeft.setCompoundDrawablePadding(this.leftIcoPadding);
    }

    public void setTitleIcoPadding(int titleIcoPadding) {
        this.titleIcoPadding = titleIcoPadding;
        mTitle.setCompoundDrawablePadding(this.titleIcoPadding);
    }

    public void setRightIcoPadding(int rightIcoPadding) {
        this.rightIcoPadding = rightIcoPadding;
        mRight.setCompoundDrawablePadding(this.rightIcoPadding);
    }

    public void setLeftIcoPaddingResources(int leftIcoPadding) {
        this.leftIcoPadding =  (int) getSize(getResources().getDimension(leftIcoPadding));
        mLeft.setCompoundDrawablePadding(this.leftIcoPadding);
    }

    public void setTitleIcoPaddingResources(int titleIcoPadding) {
        this.titleIcoPadding = (int) getSize(getResources().getDimension(titleIcoPadding));
        mTitle.setCompoundDrawablePadding(this.titleIcoPadding);
    }

    public void setRightIcoPaddingResources(int rightIcoPadding) {
        this.rightIcoPadding = (int) getSize(getResources().getDimension(rightIcoPadding));
        mRight.setCompoundDrawablePadding(this.rightIcoPadding);
    }
}
