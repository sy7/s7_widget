package com.widget.s7.listener;

import com.widget.s7.widget.DashBoard;

/**
 * 仪表盘回调
 */
public interface OnDashBoardListener {

    /**
     * 仪表盘回调
     * @param view 仪表盘
     * @param current 选中的值
     */
    void onDashBoard(DashBoard view, float current);

}
