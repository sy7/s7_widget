package com.widget_s7;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.widget.s7.widget.WheelPicker;

public class StringWheelPicker extends WheelPicker<String> {

    public StringWheelPicker(Context context) {
        super(context);
    }

    public StringWheelPicker(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public StringWheelPicker(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public String getData(String data) {
        return data;
    }
}
