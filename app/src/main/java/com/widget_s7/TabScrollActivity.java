package com.widget_s7;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.tools.s7.recycler.BaseRecyclerViewAdapter;
import com.tools.s7.recycler.holder.BaseViewHolder;
import com.widget.s7.widget.GradualTabScrollView;

import java.util.ArrayList;
import java.util.List;

public class TabScrollActivity extends AppCompatActivity {

    private GradualTabScrollView scrollView;
    private ListView list;
//    private RecyclerView recycler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_scroll);
        scrollView = findViewById(R.id.tab_scroll_s);
        list = findViewById(R.id.tab_scroll_list);
//        recycler = findViewById(R.id.tab_scroll_recycler);
        List<String> data = new ArrayList<>();
        for(int i = 0; i < 36; i++) {
            data.add("第" + i + "条数据");
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                data);
        list.setAdapter(adapter);
      /*  LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(layoutManager);
        BaseRecyclerViewAdapter<String> adapter = new BaseRecyclerViewAdapter<String>(data) {
            @Override
            protected int getLayoutId() {
                return  R.layout.simple_list_item;
            }

            @Override
            protected void bindData(BaseViewHolder holder, int i, String s) {
                holder.setText(R.id.simple_list_text, s);
            }
        };
        recycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();*/
    }
}
