package com.widget.s7.listener;

public interface OnKeyboardListener {
    void onKeyEvent(int id);
}
