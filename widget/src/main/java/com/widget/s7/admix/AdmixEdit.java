package com.widget.s7.admix;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;

import java.util.List;

public class AdmixEdit extends RecyclerView {

    private AdmixAdapter mAdapter;

    public AdmixEdit(@NonNull Context context) {
        this(context, null);
    }

    public AdmixEdit(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AdmixEdit(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (mAdapter == null) {
            mAdapter = new AdmixAdapter(context);
        }
        setLayoutManager(new LinearLayoutManager(context));
        setAdapter(mAdapter);
    }

    public void setImage(String url) {
        mAdapter.addImage(url);
    }

    public void append(String end) {
        mAdapter.append(end);
    }

    public void setDatas(List<AdmixData> datas) {
        if (!TextUtils.isEmpty(datas.get(datas.size() - 1).getUrl())) {
            datas.add(new AdmixData());
        }
        mAdapter.setDatas(datas);
    }

    public List<AdmixData> getDatas() {
        return mAdapter.getDatas();
    }

    public void notifyDataSetChanged() {
        mAdapter.notifyDataSetChanged();
    }

}
