package com.widget.s7.banner.transforms;

import android.view.View;

public class ZoomOutTranformer extends ABaseTransformer {

	@Override
	protected void onTransform(View view, float position) {
		final float scale = 1f + Math.abs(position);
		view.setScaleX(scale);
		view.setScaleY(scale);
		view.setPivotX(view.getWidth() * 0.5f);
		view.setPivotY(view.getHeight() * 0.5f);
//		view.setAlpha((position < -1f) || (position > 1f) ? 0f : (1f - (scale - 1f)));
		float alpha = 0f;
		if (position<-1f || position>1f) {
			alpha = 0f;
		} else {
			alpha = 1f-(scale-1f);
		}
		view.setAlpha(alpha);
		if (position == -1) {
			view.setTranslationX(view.getWidth() * -1);
		}
	}

}