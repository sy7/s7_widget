package com.widget_s7;

import android.app.Application;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

public class MyApp extends Application {

    private static MyApp instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static MyApp getInstance() {
        return instance;
    }

    /**
     * 设置网络图片
     * @param url
     * @param iv
     */
    public void setImage(String url, ImageView iv) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .diskCacheStrategy(DiskCacheStrategy.NONE);
        Glide.with(this)
                .load(url)
                .apply(options)
                .into(iv);
//        Picasso.with(getInstance())
//                .load(url)
//                .placeholder(R.mipmap.ic_launcher)// 默认显示图片
//                .error(R.mipmap.ic_launcher)// 加载时出现错误显示的图片
//                .fit() //  计算出最佳的大小及最佳的图片质量来进行图片展示 (  减少内存 )
//                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE) // 查看大图放弃缓存，加速内存的回收
////                .rotate(90f) //旋转90度
//                .into(iv);
    }
}
