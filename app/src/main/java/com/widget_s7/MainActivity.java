package com.widget_s7;

import android.content.Intent;
import android.inputmethodservice.KeyboardView;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.widget.s7.helper.NumberKeyboardHelper;
import com.widget.s7.listener.OnKeyboardListener;
import com.widget.s7.widget.PasswordEdit;
import com.widget.s7.widget.PasswordEditText;
import com.widget.s7.widget.PasswordText;
import com.widget.s7.widget.TitleBar;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TitleBar mBar;
    private PasswordText mPwd;
    private PasswordEditText mPaypwd;
    private PasswordEdit mPaypwdPwd;
    private KeyboardView mKeyboard;
    private NumberKeyboardHelper mHelper;

    private Button button, button1, button2, button3, button4, button5, button6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        mBar = findViewById(R.id.main_bar);
        mPwd = findViewById(R.id.pwd);
        mPaypwd = findViewById(R.id.paypwd_edit);
        mPaypwdPwd = findViewById(R.id.paypwd_pwd);
        mKeyboard = findViewById(R.id.keyboard);
        button = findViewById(R.id.button);
        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);
        button5 = findViewById(R.id.button5);
        button6 = findViewById(R.id.button6);

        mBar.setLeftIcoResources(R.mipmap.ico_delete);
        mBar.setLeftTextResources(R.string.app_name);
        mBar.setLeftIcoPadding(0);
        mBar.setLeftPadding(0);
        mBar.setLeftSizeResources(R.dimen.size_18);

        mHelper = new NumberKeyboardHelper(this);
        mHelper.setKeyboardView(mKeyboard)
//                .setEditText(mPaypwdPwd)
                .setEditText(mPaypwd)
                .setOnKeyboardListener(new OnKeyboardListener() {
                    @Override
                    public void onKeyEvent(int id) {
                        Log.d("结果", mPaypwdPwd.getText().toString());
                        mPwd.setPassword(mPaypwdPwd.getText().toString());
                    }
                }).init();

        button.setOnClickListener(this);
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button:
                mHelper.shuffleKeyboard();
                break;
            case R.id.button1:
                startActivity(new Intent(MainActivity.this, PickerActivity.class));
                break;
            case R.id.button2:
                startActivity(new Intent(MainActivity.this, TabScrollActivity.class));
                break;
            case R.id.button3:
                startActivity(new Intent(MainActivity.this, BannerActivity.class));
                break;
            case R.id.button4:
                startActivity(new Intent(MainActivity.this, ImageActivity.class));
                break;
            case R.id.button5:
                startActivity(new Intent(MainActivity.this, AdmixActivity.class));
                break;
            case R.id.button6:
                startActivity(new Intent(MainActivity.this, DashBoardActivity.class));
                break;
        }
    }

}
