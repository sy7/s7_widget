package com.widget.s7.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;

import com.widget.s7.attr.DashboardAttr;
import com.widget.s7.listener.OnDashBoardListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 自定义仪表盘
 */
public class DashBoard extends View {

    public static final int INTERVAL = 0;
    public static final int INTERVAL_EVEN = 1;
    public static final int INTERVAL_ODD = 2;

    private DashboardAttr dashBoardAttr;
    private boolean isDragging = true;
    Point point = new Point();
    private  float oldX, oldY;
    private boolean isTrunc = true;
    private boolean isDown = false;
    private int mWidth, mHeight;
    private int startColor, endColor;
    private Paint paintProgress;
    private Paint paintIndicator;
    private Paint paintIndicatorOut;
    private int indicatorColor;
    private int progressWidth;
    private int OFFSET = 30;
    private int START_ARC;
    private int DURING_ARC;
    private RectF rectF;
    private float mCurrent = 0;
    private float mStart;
    private float mEnd;
    private float mCountSpace;
    private int mCountSum;
    private int mCount;
    private int mCountGroup;
    private int mCountRule;
    private List<Float> mCountArray = null;
    private String mUnit;
    private Paint paintBackgroundOut;
    private Paint paintBackground;
    private int backgroundColor;
    private int mCenterCircleRadius;
    private Paint paintCenterCircle;
    private int centerCircleColor;
    private int mScaleY;
    private Paint paintScale;
    private Paint paintScaleStr;
    private int textColor;
    private int textSize;
    private Paint paintText;
    private Paint paintUnit;
    private ValueAnimator mAnimator;

    private OnDashBoardListener mListener;

    public DashBoard(Context context) {
        this(context, null);
    }

    public DashBoard(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DashBoard(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        dashBoardAttr = new DashboardAttr(context, attrs, defStyleAttr);
        initAttr();
        initPaint();
    }

    private void initAttr() {
        isDragging = dashBoardAttr.isDragging();
        isTrunc = dashBoardAttr.isTrunc();
        mStart = dashBoardAttr.getStart();
        mEnd = dashBoardAttr.getEnd();
        startColor = dashBoardAttr.getStartColor();
        endColor = dashBoardAttr.getEndColor();
        progressWidth = dashBoardAttr.getProgressWidth();
        DURING_ARC = dashBoardAttr.getDuringArc();
        indicatorColor = dashBoardAttr.getIndicatorColor();
        mCount = dashBoardAttr.getCount();
        mCountGroup = dashBoardAttr.getCountGroup();
        mCountRule = dashBoardAttr.getCountRule();
        mUnit = dashBoardAttr.getUnit();
        backgroundColor = dashBoardAttr.getBackgroundColor();
        centerCircleColor = dashBoardAttr.getCenterCircleColor();
        textColor = dashBoardAttr.getTextColor();
        textSize = getSize(dashBoardAttr.getTextSize());
    }

    private int getSize(float size) {
        DisplayMetrics dm = getResources().getDisplayMetrics();
        float value = dm.scaledDensity;
        return (int) (size * value);
    }

    private void initPaint() {
        paintBackgroundOut = new Paint();
        paintBackgroundOut.setAntiAlias(true);
        paintBackgroundOut.setStyle(Paint.Style.STROKE);
        paintBackgroundOut.setStrokeWidth(2);
        paintBackgroundOut.setColor(0xFFDFDFDF);
        paintBackgroundOut.setDither(true);

        paintBackground = new Paint();
        paintBackground.setAntiAlias(true);
        paintBackground.setStyle(Paint.Style.FILL);
        paintBackground.setStrokeWidth(2);
        paintBackground.setDither(true);

        paintProgress = new Paint();
        paintProgress.setAntiAlias(true);
        paintProgress.setStrokeWidth(progressWidth);
        paintProgress.setStyle(Paint.Style.STROKE);
        paintProgress.setStrokeCap(Paint.Cap.ROUND);
        paintProgress.setDither(true);

        paintIndicator = new Paint();
        paintIndicator.setAntiAlias(true);
        paintIndicator.setStyle(Paint.Style.FILL);
        paintIndicator.setDither(true);
        paintIndicator.setColor(indicatorColor);

        paintIndicatorOut = new Paint();
        paintIndicatorOut.setAntiAlias(true);
        paintIndicatorOut.setStyle(Paint.Style.STROKE);
        paintIndicatorOut.setStrokeWidth(2);
        paintIndicatorOut.setColor(0xFFDFDFDF);
        paintIndicatorOut.setDither(true);

        paintCenterCircle = new Paint();
        paintCenterCircle.setAntiAlias(true);
        paintCenterCircle.setStyle(Paint.Style.FILL);
        paintCenterCircle.setDither(true);
        paintCenterCircle.setColor(centerCircleColor);

        paintScale = new Paint();
        paintScale.setAntiAlias(true);
        paintScale.setColor(textColor);
        paintScale.setStrokeWidth(3);
        paintScale.setStyle(Paint.Style.FILL);
        paintScale.setDither(true);

        paintScaleStr = new Paint();
        paintScaleStr.setAntiAlias(true);
        paintScaleStr.setStyle(Paint.Style.FILL);
        paintScaleStr.setTextAlign(Paint.Align.LEFT);
        paintScaleStr.setColor(textColor);
        paintScaleStr.setTextSize(20);

        paintText = new Paint();
        paintText.setAntiAlias(true);
        paintText.setStyle(Paint.Style.FILL);
        paintText.setTextAlign(Paint.Align.LEFT);
        paintText.setColor(textColor);
        paintText.setTextSize(textSize);

        paintUnit = new Paint();
        paintUnit.setAntiAlias(true);
        paintUnit.setStyle(Paint.Style.FILL);
        paintUnit.setTextAlign(Paint.Align.LEFT);
        paintUnit.setColor(textColor);
        paintUnit.setTextSize(textSize / 3);
    }

    private void initShader() {
        updateOval();
        if (startColor != 0 && endColor != 0) {
            SweepGradient shader = new SweepGradient(0, 0,
                    new int[]{startColor, startColor,  endColor, endColor}, null);
            float rotate = 90f;
            Matrix gradientMatrix = new Matrix();
            gradientMatrix.preRotate(rotate, 0, 0);
            shader.setLocalMatrix(gradientMatrix);
            paintProgress.setShader(shader);
        }
    }

    private void updateOval() {
        rectF = new RectF((- mWidth / 2) + OFFSET,
                - (mHeight / 2) + OFFSET,
                (mWidth / 2) - OFFSET,
                (mWidth / 2) - OFFSET);
        START_ARC = 90 + ((360 - DURING_ARC) >> 1);
    }

    private void initCountArray() {
        if (mCountArray == null) {
            mCountArray = new ArrayList<>();
        }
        mCountArray.clear();
        mCurrent = 0;
        mCountSum = mCount * mCountGroup + 1;
        mCountSpace = (mEnd - mStart) / mCount;
        for (int i=0; i<= mCount; i++) {
            mCountArray.add(mStart + mCountSpace * i);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int specSize = MeasureSpec.getSize(Math.min(widthMeasureSpec, heightMeasureSpec));
        setMeasuredDimension(specSize, specSize);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = Math.min(h, w);
        mHeight = Math.min(h, w);
        mCenterCircleRadius = mWidth / 2 - 2 * OFFSET;
        mScaleY = - mWidth / 2 + OFFSET * 2;
        initShader();
        initCountArray();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.translate(mWidth / 2, mHeight / 2);
        drawBackground(canvas);
        drawProgress(canvas);
        drawCenter(canvas);
        drawerCount(canvas);
        drawProgressIndicator(canvas);
        drawText(canvas);
    }

    private void drawText(Canvas canvas) {
        String text = String.format("%.0f", mCurrent);
        float textX = getTextLength(paintText, String.valueOf(text)) / 2;
        paintText.setColor(textColor);
        paintText.setTextSize(textSize);
        canvas.drawText(String.valueOf(text), - textX, textSize / 2, paintText);
        if (!TextUtils.isEmpty(mUnit)) {
            paintUnit.setColor(textColor);
            paintUnit.setTextSize(textSize / 3);
            canvas.drawText(mUnit,
                    textX + getTextLength(paintUnit, mUnit) / 2,
                    textSize - textSize / 2, paintUnit);
        }
    }

    private void drawerCount(Canvas canvas) {
        canvas.save();
        canvas.rotate(-(180 - START_ARC + 90), 0, 0);
        float rAngle = DURING_ARC / ((mCountSum - 1) * 1.0f);
        paintScale.setColor(textColor);
        paintScaleStr.setColor(textColor);
        for (int i = 0; i < mCountSum; i++) {
            canvas.save();
            canvas.rotate(rAngle * i, 0, 0);
            if (i == 0 || i % mCountGroup == 0) {
                canvas.drawLine(0, mScaleY + 5, 0, mScaleY + 25, paintScale);
                drawCount(canvas, i);
            } else {
                canvas.drawLine(0, mScaleY + 5, 0, mScaleY + 15, paintScale);
            }
            canvas.restore();
        }
        canvas.restore();

    }

    private void drawCount(Canvas canvas, int i) {
        if (mCountArray.size() > (i % mCountGroup)) {
            if (mCountRule == INTERVAL_EVEN) {
                if (i / mCountGroup % 2 == 0) {
                    drawCountRule(canvas, i);
                } else {
                    if (i / mCountGroup == mCountArray.size() - 1) {
                        drawCountRule(canvas, i);
                    }
                }
            } else  if (mCountRule == INTERVAL_ODD) {
                if (i / mCountGroup % 3 == 0) {
                    drawCountRule(canvas, i);
                } else {
                    if (i / mCountGroup == mCountArray.size() - 1) {
                        drawCountRule(canvas, i);
                    }
                }
            } else {
                drawCountRule(canvas, i);
            }
        }
    }

    private void drawCountRule(Canvas canvas, int i) {
        String text = String.format("%.0f", (mCountArray.get(i / mCountGroup)));
        Paint.FontMetricsInt fontMetrics = paintScaleStr.getFontMetricsInt();
        int baseline = ((mScaleY + 40) + (fontMetrics.bottom - fontMetrics.top) / 2);
        canvas.drawText(text, - getTextLength(paintScaleStr, text) / 2, baseline, paintScaleStr);
    }

    private float getTextLength(Paint paint, String text) {
        if (TextUtils.isEmpty(text)) return 0;
        float textLength = paint.measureText(text);
        return textLength;
    }

    private void drawCenter(Canvas canvas) {
        paintCenterCircle.setColor(centerCircleColor);
        canvas.drawCircle(0, 0, mCenterCircleRadius, paintCenterCircle);
    }

    private void drawProgressIndicator(Canvas canvas) {
        float progressRadians = getSweep();
        float thumbX = ((mWidth / 2) - OFFSET) * (float) Math.sin(progressRadians);
        float thumbY = ((mHeight / 2) - OFFSET) * (float) Math.cos(progressRadians);
        point.x = (int) thumbX;
        point.y = (int) thumbY;
        canvas.drawCircle(thumbX, thumbY, progressWidth + 10 + 1, paintIndicatorOut);
        paintIndicator.setColor(indicatorColor);
        canvas.drawCircle(thumbX, thumbY, progressWidth + 10, paintIndicator);
    }

    private void drawProgress(Canvas canvas) {
        canvas.drawArc(rectF, START_ARC, DURING_ARC, false, paintProgress);
    }

    private void drawBackground(Canvas canvas) {
        canvas.drawCircle(0, 0, mWidth / 2 - 2, paintBackgroundOut);
        canvas.save();
        if (backgroundColor != 0) {
            paintBackground.setColor(backgroundColor);
            canvas.drawCircle(0, 0, mWidth / 2 - 4, paintBackground);
        }
    }

    private float getSweep() {
        float proportion = DURING_ARC * ((mEnd - mStart - (mCurrent - mStart)) / (mEnd - mStart));
        return (float) (((360.0f - DURING_ARC) / 2 + proportion) / 180 * Math.PI);
    }

    public void setOnListener(OnDashBoardListener listener) {
        this.mListener = listener;
    }

    public DashBoard setDragging(boolean dragging) {
        isDragging = dragging;
        return this;
    }

    public DashBoard setTrunc(boolean trunc) {
        isTrunc = trunc;
        return this;
    }

    public void setCurrent(float current) {
        setAnimator(current);
    }

    public DashBoard setDividingRule(float start, float end, String unit) {
        return setDividingRule(start, end, mCount, mCountGroup, unit);
    }

    public DashBoard setDividingRule(float start, float end, int count, String unit) {
        return setDividingRule(start, end, count, mCountGroup, unit);
    }

    public DashBoard setDividingRule(float start, float end, int count, int countGroup, String unit) {
        this.mStart = start;
        this.mEnd = end;
        this.mCount=count;
        this.mUnit = unit;
        this.mCountGroup = countGroup;
        return setDividingRule(start, end, count, mCountGroup, mCountRule, unit);
    }

    public DashBoard setDividingRule(float start, float end, int count, int countGroup, int countRule, String unit) {
        this.mStart = start;
        this.mEnd = end;
        this.mCount=count;
        this.mUnit = unit;
        this.mCountGroup = countGroup;
        this.mCountRule = countRule;
        initShader();
        initCountArray();
        invalidate();
        return this;
    }

    public DashBoard setProgress(int startColor, int endColor) {
        return setProgress(startColor, endColor, progressWidth);
    }

    public DashBoard setProgress(int startColor, int endColor, int width) {
        this.startColor = startColor;
        this.endColor = endColor;
        this.progressWidth = width;
        return setProgress(startColor, endColor, width, DURING_ARC);
    }

    public DashBoard setProgress(int startColor, int endColor, int width, int duringArc) {
        this.startColor = startColor;
        this.endColor = endColor;
        this.progressWidth = width;
        this.DURING_ARC = duringArc;
        initShader();
        initCountArray();
        invalidate();
        return this;
    }

    public DashBoard setIndicatorColor(int indicatorColor) {
        this.indicatorColor = indicatorColor;
        invalidate();
        return this;
    }

    public DashBoard setBackground(int backgroundColor) {
        this.backgroundColor = backgroundColor;
        invalidate();
        return this;
    }

    public DashBoard setCenterCircleColor(int centerCircleColor) {
        this.centerCircleColor = centerCircleColor;
        invalidate();
        return this;
    }

    public DashBoard setTextColor(int textColor) {
        this.textColor = textColor;
        invalidate();
        return this;
    }

    public DashBoard setTextSize(int textSize) {
        this.textSize = textSize;
        invalidate();
        return this;
    }

    private void setAnimator(final float current) {
        if (mAnimator != null && mAnimator.isRunning()) {
            mAnimator.cancel();
        }
        long time = (long) (Math.abs(current - mCurrent) / (mEnd - mStart) * 2000);
        mAnimator = ValueAnimator.ofFloat(mCurrent, current);
        mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mCurrent = (float) animation.getAnimatedValue();
                invalidate();
            }
        });
        mAnimator.setRepeatMode(ValueAnimator.REVERSE);
        mAnimator.setDuration(time);
        mAnimator.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!isDragging) {
            return super.onTouchEvent(event);
        }
        float currentX = event.getX();
        float currentY = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (isNearProgress(currentX, currentY) && isNearIndicator(currentX, currentY)) {
                    oldX = currentX;
                    oldY = currentY;
                    isDown = true;
                }
                if (isNearProgress(currentX, currentY)) {
                    oldX = currentX;
                    oldY = currentY;
                    isDown = true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (isNearProgress(currentX, currentY) && isNearIndicator(currentX, currentY)) {
                    if (isDown) {
                        oldX = currentX;
                        oldY = currentY;
                        mCurrent = calculateAngle(currentX, currentY);
                        if (mCurrent < 0) {
                            mCurrent = 0;
                        }
                        if (mCurrent > mEnd) {
                            mCurrent = mEnd;
                        }
                        invalidate();
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                if (isDown) {
                    getTrunc(calculateAngle(oldX, oldY));
                }
                isDown = false;
                break;
        }
        return true;
    }

    private float calculateAngle(float x, float y) {
        float degrees;
        if (x >= mWidth / 2) {
            degrees = ((float) (DURING_ARC + 60 - 180 * (Math.atan2(x - mWidth / 2, y -  mHeight / 2)) / Math.PI));
        } else {
            degrees = ((float) (- 60 + 180 * (Math.atan2(mWidth / 2 - x, y - mHeight / 2)) / Math.PI));
        }
        return (mEnd - mStart) * degrees / DURING_ARC;

    }

    private void getTrunc(float current) {
        if (isTrunc) {
            int num = (int) (current / mCountSpace);
            if (current % mCountSpace >= mCountSpace / 2) {
                current = mCountArray.get(num + 1);
            } else {
                current = mCountArray.get(num);
            }
        }
        if (mListener != null) {
            mListener.onDashBoard(this, current);
        }
        setAnimator(current);
    }

    private boolean isNearProgress(float x, float y) {
        float distance = (float) Math.sqrt(Math.pow((mWidth / 2 - x), 2) + Math.pow((mHeight / 2 - y), 2));
        if (distance < mWidth / 2 - OFFSET + progressWidth / 2 + 30
                && distance > mWidth / 2 - OFFSET - progressWidth / 2 - 30) {
            if (y <= calculate4Y(START_ARC)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private boolean isNearIndicator(float x, float y) {
        return getCircleX(x) < point.x + progressWidth + 5
                && getCircleX(x) > point.x - progressWidth - 5
                && getCircleY(y) < point.y + progressWidth + 5
                && getCircleY(y) > point.y - progressWidth - 5;
    }

    private float getCircleX(float x) {
        if (x > mWidth / 2) {
            return x - mWidth / 2;
        } else if(x < mWidth / 2) {
           return - mWidth / 2 + x;
        }
        return 0;
    }

    private float getCircleY(float y) {
        if (y > mHeight / 2) {
           return y - mHeight / 2;
        } else if(y < mHeight / 2) {
            return - mHeight / 2 + y;
        }
        return 0;
    }

    private float calculate4Y(double angle) {
        angle = angle * Math.PI / 180;
        double y = (mWidth / 2 - OFFSET - progressWidth / 2 + progressWidth) * Math.sin(angle);
        double yFinal = mHeight / 2 + y;
        return (float) yFinal;
    }
}
