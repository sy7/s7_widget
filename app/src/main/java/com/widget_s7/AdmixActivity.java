package com.widget_s7;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.widget.s7.admix.AdmixData;
import com.widget.s7.admix.AdmixEdit;

import java.util.ArrayList;
import java.util.List;

public class AdmixActivity extends AppCompatActivity implements View.OnClickListener {

    private AdmixEdit admixEdit;
    private Button btn1, btn2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admix);

        admixEdit = findViewById(R.id.admix);
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);

        admixEdit.setImage("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1558435839449&di=46b5d04110f71f5702d4939982d1b403&imgtype=0&src=http%3A%2F%2Fimg1.ph.126.net%2FyNsQ4xEq9ge0XYL8k1V74Q%3D%3D%2F6608827944004142722.jpg");
        List<AdmixData> dataList = new ArrayList<>();
        for (int i = 0; i< 6; i++) {
            AdmixData data = new AdmixData();
            if (i == 0) {
                data.setInput("发发独守空房" + i);
            }
            if (i == 1) {
                data.setUrl("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1558435820011&di=d2dc2a301e6cf6aa9241153b778665dd&imgtype=0&src=http%3A%2F%2Fimg9.ph.126.net%2FFCkNjK5Q191n5hlLqkMO3w%3D%3D%2F627689198082459729.jpg");
            }
            if (i == 2) {
                data.setInput("发发独守空房" + i);
            }
            if (i == 3) {
                data.setUrl("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1558435783717&di=ffd25852d5686bd5e208c5001f00a85f&imgtype=0&src=http%3A%2F%2Fimg17.3lian.com%2Fd%2Ffile%2F201702%2F09%2F42b8ab61bbf35a3d0dd9428745946874.jpg");
            }
            if (i == 4) {
                data.setInput("发发独守空房" + i);
            }
            if (i == 5) {
                data.setUrl("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1558435839449&di=46b5d04110f71f5702d4939982d1b403&imgtype=0&src=http%3A%2F%2Fimg1.ph.126.net%2FyNsQ4xEq9ge0XYL8k1V74Q%3D%3D%2F6608827944004142722.jpg");
            }
            dataList.add(data);
        }
        admixEdit.setDatas(dataList);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn1:
                admixEdit.setImage("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1558435839449&di=46b5d04110f71f5702d4939982d1b403&imgtype=0&src=http%3A%2F%2Fimg1.ph.126.net%2FyNsQ4xEq9ge0XYL8k1V74Q%3D%3D%2F6608827944004142722.jpg");
                break;
            case R.id.btn2:
                admixEdit.notifyDataSetChanged();
                break;
        }
    }
}
