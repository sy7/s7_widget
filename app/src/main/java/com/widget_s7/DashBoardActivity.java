package com.widget_s7;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;

import com.widget.s7.listener.OnDashBoardListener;
import com.widget.s7.test.DashboardView;
import com.widget.s7.widget.DashBoard;
import com.widget.s7.test.TempHumView;

public class DashBoardActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener,
        OnDashBoardListener, View.OnClickListener {

    private TempHumView mTempHum;
    private DashboardView dashboardView;
    private SeekBar seekBar;
    private DashBoard dashBoard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        mTempHum = findViewById(R.id.dash_board_temp_hum);

        dashboardView = findViewById(R.id.dash_board_dashboard);
        seekBar = findViewById(R.id.dash_board_dashboard_seek);

        dashBoard = findViewById(R.id.dash_board_dash);
        dashBoard.setOnListener(this);
        seekBar.setOnSeekBarChangeListener(this);
        findViewById(R.id.dash_board_btn1).setOnClickListener(this);
        findViewById(R.id.dash_board_btn2).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dash_board_btn1:
                dashBoard.setDividingRule(0, 240, 12, 3, DashBoard.INTERVAL_ODD, "℃");
                break;
            case R.id.dash_board_btn2:
                dashBoard.setDividingRule(0, 2200, 11, 3, DashBoard.INTERVAL_ODD, "w");
                break;
        }
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        dashboardView.setPercent(progress);
        mTempHum.setHum(progress);
        mTempHum.setTemp(progress - 40);
        float current = progress  * 240 / 100;
        Log.d("进度", "当前温度：" + current);
        dashBoard.setCurrent(current);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onDashBoard(DashBoard view, float current) {
        Log.e("activity 中选中值回调", String.valueOf(current));
    }

}
