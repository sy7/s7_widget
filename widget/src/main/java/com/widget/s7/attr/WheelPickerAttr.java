package com.widget.s7.attr;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.ColorInt;
import android.util.AttributeSet;
import android.util.DisplayMetrics;

import com.widget.s7.R;
import com.widget.s7.utils.PxUtil;

public class WheelPickerAttr {

    @ColorInt
    private int mUnSelectedColor;
    private int mUnSelectedSize;
    private boolean isGradual;

    @ColorInt
    private int mSelectedColor;
    private int mSelectedSize;

    private String mIndicator;
    @ColorInt
    private int mIndicatorColor;
    private int mIndicatorSize;
    private int mSpacing;

    private int mHalfCount;
    private int mCurrent;

    private boolean isZoom;

    private boolean isCurtain;

    @ColorInt
    private int mCurtainColor;

    private boolean isCurtainBorder;

    @ColorInt
    private int mCurtainBorderColor;

    private boolean isCyclic;

    public WheelPickerAttr(Context context, AttributeSet attrs, int defStyleAttr) {
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.WheelPicker, defStyleAttr, 0);

        mHalfCount = ta.getInteger(R.styleable.WheelPicker_visibleNumber, 2);
        mCurrent = ta.getInteger(R.styleable.WheelPicker_currentPosition, 0);

        mSelectedColor = ta.getColor(R.styleable.WheelPicker_selectedColor, 0xFF33AAFF);
        mSelectedSize = getSize(context, ta.getDimensionPixelSize(PxUtil.spToPx(R.styleable.WheelPicker_selectedSize, context), 22));

        mUnSelectedColor = ta.getColor(R.styleable.WheelPicker_unSelectedColor, 0xFF333333);
        mUnSelectedSize = getSize(context, ta.getDimensionPixelSize(PxUtil.spToPx(R.styleable.WheelPicker_unSelectedSize, context), 18));

        isGradual = ta.getBoolean(R.styleable.WheelPicker_isGradual, true);
        isZoom = ta.getBoolean(R.styleable.WheelPicker_isZoom, true);
        isCyclic = ta.getBoolean(R.styleable.WheelPicker_isCyclic, false);
        mSpacing = getSize(context, ta.getDimensionPixelSize(PxUtil.spToPx(R.styleable.WheelPicker_spacing, context), 10));

        isCurtain = ta.getBoolean(R.styleable.WheelPicker_isCurtain, true);
        mCurtainColor = ta.getColor(R.styleable.WheelPicker_curtainColor, 0xFFFFFFFF);
        isCurtainBorder = ta.getBoolean(R.styleable.WheelPicker_curtainBorder, true);
        mCurtainBorderColor = ta.getColor(R.styleable.WheelPicker_curtainBorderColor, 0xFF33AAFF);

        mIndicator = ta.getString(R.styleable.WheelPicker_indicator);
        mIndicatorColor = ta.getColor(R.styleable.WheelPicker_indicatorColor, 0xFF33AAFF);
        mIndicatorSize = getSize(context, ta.getDimensionPixelSize(PxUtil.spToPx(R.styleable.WheelPicker_indicatorSize, context), 18));
        ta.recycle();
    }

    private int getSize(Context context, float size) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        float value = dm.scaledDensity;
        return (int) (size * value);
    }

    public int getUnSelectedColor() {
        return mUnSelectedColor;
    }

    public int getUnSelectedSize() {
        return mUnSelectedSize;
    }

    public boolean isGradual() {
        return isGradual;
    }

    public int getSelectedColor() {
        return mSelectedColor;
    }

    public int getSelectedSize() {
        return mSelectedSize;
    }

    public String getIndicator() {
        return mIndicator;
    }

    public int getIndicatorColor() {
        return mIndicatorColor;
    }

    public int getIndicatorSize() {
        return mIndicatorSize;
    }

    public int getSpacing() {
        return mSpacing;
    }

    public int getHalfCount() {
        return mHalfCount;
    }

    public int getCurrent() {
        return mCurrent;
    }

    public boolean isZoom() {
        return isZoom;
    }

    public boolean isCurtain() {
        return isCurtain;
    }

    public int getCurtainColor() {
        return mCurtainColor;
    }

    public boolean isCurtainBorder() {
        return isCurtainBorder;
    }

    public int getCurtainBorderColor() {
        return mCurtainBorderColor;
    }

    public boolean isCyclic() {
        return isCyclic;
    }
}
